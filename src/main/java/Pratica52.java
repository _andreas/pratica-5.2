
import utfpr.ct.dainf.if62c.pratica.AZero;
import utfpr.ct.dainf.if62c.pratica.DeltaN;
import utfpr.ct.dainf.if62c.pratica.Equacao2Grau;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná DAINF - Departamento
 * Acadêmico de Informática IF62C - Fundamentos de Programação 2
 *
 * Template de projeto de programa Java usando Maven.
 *
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica52 {

    public static void main(String[] args) {
        Equacao2Grau a = null, b = null, c = null, d = null;
        try {
            a = new Equacao2Grau(0, 2, 3);
        } catch (AZero | DeltaN ex) {
            System.out.println(ex.getLocalizedMessage());
        }
        try {
            b = new Equacao2Grau(1, 5, 2);
            System.out.println(b.getRaiz1());
            System.out.println(b.getRaiz2());
        } catch (AZero | DeltaN ex) {
            System.out.println(ex.getLocalizedMessage());
        }
        try {
            c = new Equacao2Grau(4, 24, 36);
            System.out.println(c.getRaiz1());
            System.out.println(c.getRaiz2());
        } catch (AZero | DeltaN ex) {
            System.out.println(ex.getLocalizedMessage());
        }
        try {
            d = new Equacao2Grau(1, 2, 3);
            System.out.println(d.getRaiz1());
            System.out.println(d.getRaiz2());
        } catch (AZero | DeltaN ex) {
            System.out.println(ex.getLocalizedMessage());
        }

    }
}
